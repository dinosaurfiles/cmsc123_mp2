import java.util.*;

/**
 * Main class of Dijkstras Algorithm
 */
public class DijkstrasProto {
   PriorityQueue PQ;

   /**
    * Main code based on the slides pseudo code.
    *@param Graph graph. calls and initialize the Graph class
    *@param int source. source or from which node it starts
    *@param int arraySize. number of vertex
    */
   public void dijkstra(Graph graph, int source, int arraySize){
      PQ = new PriorityQueue(arraySize);

      for (int i = 0; i < arraySize; i++) {
         graph.pred[i] = 0;
      }

      initPQ(graph, PQ, source, arraySize);
      graph.pred[0] = 0;

      while (!isEmptyPQ(PQ)) {

         int u = extractMin(PQ);

         if (PQ.key[u - 1] == Double.POSITIVE_INFINITY) {
            break;
         }

         Iterator<Integer> aa = graph.vertices.get(u - 1).posOfAdj.iterator();
         Iterator<Double>  cc = graph.vertices.get(u - 1).cost.iterator();

         while (aa.hasNext()) {
            int    a = (int)aa.next();
            int    v = a;
            double c = cc.next();
            double newval = PQ.key[u - 1] + c;

            if (PQ.key[v - 1] > newval) {
               graph.pred[v - 1] = u;
               decreaseKey(PQ, v, newval);
            }
         }
      }

      for (int i = 0; i < arraySize; i++) {
         int a = graph.pred[i];

         if (i + 1 != source) {
            if (a == 0) {
               System.out.println("No path from " + source + " to " + (i + 1) + " ");
            } else {
               System.out.print("Vertex " + source + " to Vertex " + (i + 1) + " : " + source + " -> ");

               if (a == source) {
                  System.out.println((i + 1) + " (cost = " + PQ.key[i] + ")");
               } else {
                  while (true) {
                     if (a != source) {
                        System.out.print(a + " -> ");
                        a = graph.pred[a - 1];
                     } else {
                        System.out.println(i + 1 + " (cost = " + PQ.key[i] + ")");
                        break;
                     }
                  }
               }
            }
         }
      }
   }

   /**
    * Method that extracts the minimum vertex from the heap
    *@param PriorityQueue PQ. calls the PriorityQueue
    */
   public int extractMin(PriorityQueue PQ){
      int j = 0;

      if (PQ.sizePQ == 0) {
      } else {
         j = PQ.heap[0];
         PQ.heap[0] = PQ.heap[PQ.sizePQ - 1];
         PQ.index[PQ.heap[0] - 1] = 1;
         PQ.sizePQ = PQ.sizePQ - 1;
         heapify(PQ, 1);
      }

      return j;
   }

   /**
    * Method that heapify from the given graph
    *@param PriorityQueue PQ. the PriorityQueue
    *@param int r. the r vertex
    */
   public void heapify(PriorityQueue PQ, int r){
      double k = PQ.key[PQ.heap[r - 1] - 1];
      int    l = PQ.heap[r - 1];
      int    i = r;
      int    j = 2 * i;

      while (j <= PQ.sizePQ) {
         if ((j < PQ.sizePQ) && (PQ.key[PQ.heap[j] - 1] < PQ.key[PQ.heap[j - 1] - 1])) {
            j++;
         }

         if (PQ.key[PQ.heap[j - 1] - 1] < k) {
            PQ.heap[i - 1] = PQ.heap[j - 1];
            PQ.index[PQ.heap[j - 1] - 1] = i;
            i = j;
            j = 2 * i;
         } else {
            break;
         }
      }

      PQ.heap[i - 1]  = l;
      PQ.index[l - 1] = i;
   }

   /**
    * Method that decreases the key
    *@param PriorityQueue PQ. PriorityQueue
    *@param int l.
    *@param double newkey. "inserts" the newkey
    */
   public void decreaseKey(PriorityQueue PQ, int l, double newkey){
      PQ.key[l - 1] = newkey;
      int i = PQ.index[l - 1];
      int j = i / 2;

      while (i > 1 && PQ.key[PQ.heap[j - 1] - 1] > newkey) {
         PQ.heap[i - 1] = PQ.heap[j - 1];
         PQ.index[PQ.heap[j - 1] - 1] = i;
         i = j;
         j = i / 2;
      }

      PQ.heap[i - 1]  = l;
      PQ.index[l - 1] = i;
   }

   /**
    * Initialize the PriorityQueue
    *@param Graph graph. the graph
    *@param PriorityQueue PQ. PriorityQueue
    *@param int source. from where source
    *@param int arraySize. num of vertex
    */
   public void initPQ(Graph graph, PriorityQueue PQ, int source, int arraySize){

      int i = 1;

      for (int v = 1; v <= arraySize; v++) {
         if (v == source) {
            PQ.heap[0] = source;
            PQ.index[source - 1] = 1;
            PQ.key[source - 1]   = 0;
         } else {
            i += 1;
            PQ.heap[i - 1]  = v;
            PQ.index[v - 1] = i;
            PQ.key[v - 1]   = Integer.MAX_VALUE;
         }

         PQ.sizePQ = arraySize;
      }
   }

   public boolean isEmptyPQ(PriorityQueue PQ){
      return (PQ.sizePQ == 0);
   }

}

/**
*Main class pf the PriorityQueue
*/
class PriorityQueue {
   int[]    heap;
   int[]    index;
   double[] key;
   int sizePQ;
   /**
    *Method that sets the PriorityQueue
    *@param int size. sets the size of the heap index and key
    */
   PriorityQueue(int size){
      heap  = new int[size];
      index = new int[size];
      key   = new double[size];
   }

}
