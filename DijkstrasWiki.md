
[Source](https://en.wikipedia.org/wiki/Dijkstra's_algorithm "Permalink to Dijkstra's algorithm - Wikipedia, the free encyclopedia")

# Dijkstra's algorithm - Wikipedia, the free encyclopedia

Dijkstra's algorithm
| ----- |
| ![Dijkstra's algorithm runtime][1]

Dijkstra's algorithm. It picks the unvisited vertex with the lowest-distance, calculates the distance through it to each unvisited neighbor, and updates the neighbor's distance if smaller. Mark visited (set to red) when done with neighbors.

 |
| Class |  [Search algorithm][2] |
| Data structure |  [Graph][3] |
| [Worst case performance][4] |  ![O\(|E| + |V| log|V|\)][5] |

**Dijkstra's algorithm** is an [algorithm][6] for finding the [shortest paths][7] between [nodes][8] in a graph, which may represent, for example, road networks. It was conceived by [computer scientist][9] [Edsger W. Dijkstra][10] in 1956 and published three years later.[1][2] The algorithm exists in many variants; Dijkstra's original variant found the shortest path between two nodes,[2] but a more common variant fixes a single node as the "source" node and finds shortest paths from the source to all other nodes in the graph, producing a [shortest path tree][11].

For a given source node in the graph, the algorithm finds the shortest path between that node and every other.[3]:196–206 It can also be used for finding the shortest paths from a single node to a single destination node by stopping the algorithm once the shortest path to the destination node has been determined. For example, if the nodes of the graph represent cities and edge path costs represent driving distances between pairs of cities connected by a direct road, Dijkstra's algorithm can be used to find the shortest route between one city and all other cities. As a result, the shortest path algorithm is widely used in network [routing protocols][12], most notably [IS-IS][13] and Open Shortest Path First ([OSPF][14]). It is also employed as a [subroutine][15] in other algorithms such as [Johnson's][16].

Dijkstra's original algorithm does not use a [min-priority queue][17] and runs in [time][18] ![O\(|V|^2\)][19] (where ![|V|][20] is the number of nodes). The idea of this algorithm is also given in (Leyzorek et al. 1957). The implementation based on a min-priority queue implemented by a [Fibonacci heap][21] and running in ![O\(|E|+|V|log|V|\)][5] (where ![|E|][22] is the number of edges) is due to (Fredman &amp; Tarjan 1984). This is [asymptotically][23] the fastest known single-source [shortest-path algorithm][7] for arbitrary [directed graphs][24] with unbounded non-negative weights.

In some fields, [artificial intelligence][25] in particular, Dijkstra's algorithm or a variant of it is known as **uniform-cost search** and formulated as an instance of the more general idea of [best-first search][26].[4]

## History[[edit][27]]

Dijkstra thought about the shortest path problem when working at the Mathematical Center in Amsterdam in 1956 as a programmer to demonstrate capabilities of a new computer called ARMAC. His objective was to choose both a problem as well as an answer (that would be produced by computer) that non-computing people could understand. He designed the shortest path algorithm in about 20 minutes without aid of paper and pen and later implemented it for ARMAC for a slightly simplified transportation map of 64 cities in Netherland (ARMAC was a 6-bit computer and hence could hold 64 cities comfortably).[1] A year later, he came across another problem from hardware engineers working on the institute's next computer: minimize the amount of wire needed to connect the pins on the back panel of the machine. As a solution, he re-discovered the algorithm known as [Prim's minimal spanning tree algorithm][28] (known earlier to Jarník, and also rediscovered by Prim).[5][6] Dijkstra published the algorithm in 1959, two years after Prim and 29 years after Jarník.[7][8]

## Algorithm[[edit][29]]

![][30]

Illustration of Dijkstra's algorithm search for finding path from a start node (lower left, red) to a goal node (upper right, green) in a [robot][31] [motion planning][32] problem. Open nodes represent the "tentative" set. Filled nodes are visited ones, with color representing the distance: the greener, the farther. Nodes in all the different directions are explored uniformly, appearing as a more-or-less circular [wavefront][33] as Dijkstra's algorithm uses a [heuristic][34] identically equal to 0.

Let the node at which we are starting be called the **initial node**. Let the **distance of node _Y_** be the distance from the **initial node** to _Y_. Dijkstra's algorithm will assign some initial distance values and will try to improve them step by step.

1. Assign to every node a tentative distance value: set it to zero for our initial node and to infinity for all other nodes.
2. Set the initial node as current. Mark all other nodes unvisited. Create a set of all the unvisited nodes called the _unvisited set_.
3. For the current node, consider all of its unvisited neighbors and calculate their _tentative_ distances. Compare the newly calculated _tentative_ distance to the current assigned value and assign the smaller one. For example, if the current node _A_ is marked with a distance of 6, and the edge connecting it with a neighbor _B_ has length 2, then the distance to _B_ (through _A_) will be 6 + 2 = 8. If B was previously marked with a distance greater than 8 then change it to 8. Otherwise, keep the current value.
4. When we are done considering all of the neighbors of the current node, mark the current node as visited and remove it from the _unvisited set_. A visited node will never be checked again.
5. If the destination node has been marked visited (when planning a route between two specific nodes) or if the smallest tentative distance among the nodes in the _unvisited set_ is infinity (when planning a complete traversal; occurs when there is no connection between the initial node and remaining unvisited nodes), then stop. The algorithm has finished.
6. Otherwise, select the unvisited node that is marked with the smallest tentative distance, set it as the new "current node", and go back to step 3.

## Description[[edit][35]]

: _**Note:** For ease of understanding, this discussion uses the terms **intersection**, **road** and **map** — however, in formal terminology these terms are **vertex**, **edge** and **graph**, respectively._

Suppose you would like to find the shortest path between two [intersections][36] on a city map, a starting point and a destination. The order is conceptually simple: to start, mark the distance to every intersection on the map with infinity. This is done not to imply there is an infinite distance, but to note that intersection has not yet been _visited_; some variants of this method simply leave the intersection unlabeled. Now, at each iteration, select a _current_ intersection. For the first iteration, the current intersection will be the starting point and the distance to it (the intersection's label) will be zero. For subsequent iterations (after the first), the current intersection will be the closest unvisited intersection to the starting point—this will be easy to find.

From the current intersection, update the distance to every unvisited intersection that is directly connected to it. This is done by determining the sum of the distance between an unvisited intersection and the value of the current intersection, and [relabeling][37] the unvisited intersection with this value if it is less than its current value. In effect, the intersection is relabeled if the path to it through the current intersection is shorter than the previously known paths. To facilitate shortest path identification, in pencil, mark the road with an arrow pointing to the relabeled intersection if you label/relabel it, and erase all others pointing to it. After you have updated the distances to each [neighboring intersection][38], mark the current intersection as _visited_ and select the unvisited intersection with lowest distance (from the starting point) – or the lowest label—as the current intersection. Nodes marked as visited are labeled with the shortest path from the starting point to it and will not be revisited or returned to.

Continue this process of updating the neighboring intersections with the shortest distances, then marking the current intersection as visited and moving onto the closest unvisited intersection until you have marked the destination as visited. Once you have marked the destination as visited (as is the case with any visited intersection) you have determined the shortest path to it, from the starting point, and can trace your way back, following the arrows in reverse.

This algorithm makes no attempt to direct "exploration" towards the destination as one might expect. Rather, the sole consideration in determining the next "current" intersection is its distance from the starting point. This algorithm therefore expands outward from the starting point, interactively considering every node that is closer in terms of shortest path distance until it reaches the destination. When understood in this way, it is clear how the algorithm necessarily finds the shortest path. However, it may also reveal one of the algorithm's weaknesses: its relative slowness in some topologies.

## Pseudocode[[edit][39]]

In the following algorithm, the code u ← vertex in Q with min dist[u], searches for the vertex u in the vertex set Q that has the least dist[u] value. length(u, v) returns the length of the edge joining (i.e. the distance between) the two neighbor-nodes u and v. The variable alt on line 19 is the length of the path from the root node to the neighbor node v if it were to go through u. If this path is shorter than the current shortest path recorded for v, that current path is replaced with this alt path. The prev array is populated with a pointer to the "next-hop" node on the source graph to get the shortest route to the source.

```
     1  function Dijkstra(_Graph_, source):
     2
     3      dist[source] ← 0                       // Distance from source to source_
     4      prev[source] ← undefined               // Previous node in optimal path initialization_
     5
     6      for each vertex v in _Graph_:  // Initialization_
     7          if v ≠ source            // Where v has not yet been removed from Q (unvisited nodes)_
     8              dist[v] ← infinity             // Unknown distance function from source to v_
     9              prev[v] ← undefined            // Previous node in optimal path from source_
    10          end if
    11          add v to Q                     // All nodes initially in Q (unvisited nodes)_
    12      end for
    13
    14      while Q is not empty:
    15          u ← vertex in Q with min dist[u]  // Source node in first case_
    16          remove u from Q
    17
    18          for each neighbor v of u:           // where v is still in Q._
    19              alt ← dist[u] + length(u, v)
    20              if alt &lt; dist[v]:               // A shorter path to v has been found_
    21                  dist[v] ← alt
    22                  prev[v] ← u
    23              end if
    24          end for
    25      end while
    26
    27      return dist[], prev[]
    28
    29  end function
```
If we are only interested in a shortest path between vertices source and target, we can terminate the search after line 15 if u = target. Now we can read the shortest path from source to target by reverse iteration:

    1  _S_ ← empty sequence
    2  u ← _target_
    3  **while** prev[u] is defined:                 // Construct the shortest path with a stack S_
    4      insert u at the beginning of _S_          // Push the vertex onto the stack_
    5      u ← prev[u]                             // Traverse from target to source_
    6  **end while**

Now sequence S is the list of vertices constituting one of the shortest paths from source to target, or the empty sequence if no path exists.

A more general problem would be to find all the shortest paths between source and target (there might be several different ones of the same length). Then instead of storing only a single node in each entry of prev[] we would store all nodes satisfying the relaxation condition. For example, if both r and source connect to target and both of them lie on different shortest paths through target (because the edge cost is the same in both cases), then we would add both r and source to prev[target]. When the algorithm completes, prev[] data structure will actually describe a graph that is a subset of the original graph with some edges removed. Its key property will be that if the algorithm was run with some starting node, then every path from that node to any other node in the new graph will be the shortest path between those nodes in the original graph, and all paths of that length from the original graph will be present in the new graph. Then to actually find all these shortest paths between two given nodes we would use a path finding algorithm on the new graph, such as [depth-first search][40].

### Using a priority queue[[edit][41]]

A [min-priority queue][17] is an abstract data structure that provides 3 basic operations&nbsp;: add_with_priority(), decrease_priority() and extract_min(). As mentioned earlier, using such a data structure can lead to faster computing times than using a basic queue. Notably, [Fibonacci heap][21] (Fredman &amp; Tarjan 1984) or [Brodal queue][42] offer optimal implementations for those 3 operations. As the algorithm is slightly different, we mention it here, in pseudo-code as well&nbsp;:

    1  **function** Dijkstra(_Graph_, source):
    2      dist[source] ← 0                             // Initialization_
    3      **for each** vertex v in _Graph_:
    4          **if** v ≠ source
    5              dist[v] ← infinity                             // Unknown distance from source to v_
    6              prev[v] ← undefined                             // Predecessor of v_
    7          **end if**
    8          Q.add_with_priority(v, dist[v])
    9      **end for**
    10
    11     **while** Q is not empty:                             // The main loop_
    12         u ← Q.extract_min()                             // Remove and return best vertex_
    13         **for each** neighbor v of u:
    14             alt = dist[u] + length(u, v)
    15             **if** alt &lt; dist[v]
    16                 dist[v] ← alt
    17                 prev[v] ← u
    18                 Q.decrease_priority(v, alt)
    19             **end if**
    20         **end for**
    21     **end while**
    21     **return** dist[], prev[]

Instead of filling the priority queue with all nodes in the initialization phase, it is also possible to initialize it to contain only source; then, inside the **if** alt &lt; dist[v] block, the node must be inserted if not already in the queue (instead of performing a decrease_priority operation).[3]:198

Other data structures can be used to achieve even faster computing times in practice.[9]

## Proof of correctness[[edit][43]]

Proof is by induction on the number of visited nodes.

Invariant hypothesis: For each visited node u, dist[u] is the shortest distance from source to u; and for each unvisited v, dist[v] is the shortest distance via visited nodes only from source to v (if such a path exists, otherwise infinity; note we do not assume dist[v] is the actual shortest distance for unvisited nodes).

The base case is when there is just one visited node, namely the initial node source, and the hypothesis is trivial.

Assume the hypothesis for _n-1_ visited nodes. Now we choose an edge uv where v has the least dist[v] of any unvisited node and the edge uv is such that {{{1}}}. dist[v] must be the shortest distance from source to v because if there were a shorter path, and if w was the first unvisited node on that path then by hypothesis dist[w] &gt; dist[v] creating a contradiction. Similarly if there was a shorter path to v without using unvisited nodes then dist[v] would have been less than dist[u] + length[u,v].

After processing v it will still be true that for each unvisited node w, dist[w] is the shortest distance from source to w using visited nodes only, since if there were a shorter path which doesn't visit v we would have found it previously, and if there is a shorter path using v we update it when processing v .

## Running time[[edit][44]]

Bounds of the running time of Dijkstra's algorithm on a graph with edges ![E][45] and vertices ![V][46] can be expressed as a function of the number of edges, denoted ![|E|][22], and the number of vertices, denoted ![|V|][20], using [big-O notation][47]. How tight a bound is possible depends on the way the vertex set ![Q][48] is implemented. In the following, upper bounds can be simplified because ![|E| = O\(|V|^2\)][49] for any graph, but that simplification disregards the fact that in some problems, other upper bounds on ![|E|][22] may hold.

For any implementation of the vertex set ![Q][48], the running time is in

: ![O\(|E| cdot T_mathrm{dk} + |V| cdot T_mathrm{em}\)][50],

where ![T_mathrm{dk}][51] and ![T_mathrm{em}][52] are the complexities of the _decrease-key_ and _extract-minimum_ operations in ![Q][48], respectively. The simplest implementation of the Dijkstra's algorithm stores the vertex set ![Q][48] as an ordinary linked list or array, and extract-minimum is simply a linear search through all vertices in ![Q][48]. In this case, the running time is ![O\(|E| + |V|^2\) = O\(|V|^2\)][53].

For [sparse graphs][54], that is, graphs with far fewer than ![|V|^2][55] edges, Dijkstra's algorithm can be implemented more efficiently by storing the graph in the form of [adjacency lists][56] and using a [self-balancing binary search tree][57], [binary heap][58], [pairing heap][59], or [Fibonacci heap][21] as a [priority queue][60] to implement extracting minimum efficiently. To perform decrease-key steps in a binary heap efficiently, it is necessary to use an auxiliary data structure that maps each vertex to its position in the heap, and to keep this structure up to date as the priority queue ![Q][48] changes. With a self-balancing binary search tree or binary heap, the algorithm requires

: ![Theta\(\(|E| + |V|\) log |V|\)][61]

time in the worst case; for connected graphs this time bound can be simplified to ![Theta\( | E | log | V | \)][62]. The [Fibonacci heap][21] improves this to

: ![O\(|E| + |V| log|V|\)][5].

When using binary heaps, the [average case][4] time complexity is lower than the worst-case: assuming edge costs are drawn independently from a common [probability distribution][63], the expected number of _decrease-key_ operations is bounded by ![O\(|V| log \(|E|/|V|\)\)][64], giving a total running time of[3]:199–200

: ![O\(|E| + |V| log frac{|E|}{|V|} log |V|\)][65].

### Practical optimizations and infinite graphs[[edit][66]]

In common presentations of Dijkstra's algorithm, initially all nodes are entered into the priority queue. This is, however, not necessary: the algorithm can start with a priority queue that contains only one item, and insert new items as they are discovered (instead of doing a decrease-key, check whether the key is in the queue; if it is, decrease its key, otherwise insert it).[3]:198 This variant has the same worst-case bounds as the common variant, but maintains a smaller priority queue in practice, speeding up the queue operations.[4]

Moreover, not inserting all nodes in a graph makes it possible to extend the algorithm to find the shortest path from a single source to the closest of a set of target nodes on infinite graphs or those too large to represent in memory. The resulting algorithm is called _uniform-cost search_ (UCS) in the artificial intelligence literature[4][10][11] and can be expressed in pseudocode as

    **procedure** _UniformCostSearch_(Graph, start, goal)
      node ← start
      cost ← 0
      frontier ← priority queue containing node only
      explored ← empty set
      **do**
        **if** frontier is empty
          **return** failure
        node ← frontier.pop()
        **if** node is goal
          **return** solution
        explored.add(node)
        **for each** of node's neighbors n
          **if** n is not in explored
            **if** n is not in frontier
              frontier.add(n)
            **else if** n is in frontier with higher cost
              replace existing node with n

The complexity of this algorithm can be expressed in an alternative way for very large graphs: when _C_* is the length of the shortest path from the start node to any node satisfying the "goal" predicate, and each edge has cost at least ε, then the algorithm's worst-case time and space complexity are both in _O_(_b_1+⌊_C_*⁄ _ε_⌋).[10]

### Specialized variants[[edit][67]]

When arc weights are integers and bounded by a constant _C_, the usage of a special priority queue structure by Van Emde Boas etal.(1977) (Ahuja et al. 1990) brings the complexity to ![O\(|E|loglog|C|\)][68]. Another interesting implementation based on a combination of a new [radix heap][69] and the well-known Fibonacci heap runs in time ![O\(|E|+|V|sqrt{log|C|}\)][70] (Ahuja et al. 1990). Finally, the best algorithms in this special case are as follows. The algorithm given by (Thorup 2000) runs in ![O\(|E|loglog|V|\)][71] time and the algorithm given by (Raman 1997) runs in ![O\(|E| + |V|min{\(log|V|\)^{1/3+epsilon}, \(log|C|\)^{1/4+epsilon}}\)][72] time.

Also, for [directed acyclic graphs][73], it is possible to find shortest paths from a given starting vertex in linear ![O\(|E|+|V|\)][74] time, by processing the vertices in a topological order, and calculating the path length for each vertex to be the minimum length obtained via any of its incoming edges.[12][13]

In the special case of integer weights and undirected graphs, the Dijkstra's algorithm can be completely countered with a linear ![O\(|V|+|E|\)][75] complexity algorithm, given by (Thorup 1999).

## Related problems and algorithms[[edit][76]]

The functionality of Dijkstra's original algorithm can be extended with a variety of modifications. For example, sometimes it is desirable to present solutions which are less than mathematically optimal. To obtain a ranked list of less-than-optimal solutions, the optimal solution is first calculated. A single edge appearing in the optimal solution is removed from the graph, and the optimum solution to this new graph is calculated. Each edge of the original solution is suppressed in turn and a new shortest-path calculated. The secondary solutions are then ranked and presented after the first optimal solution.

Dijkstra's algorithm is usually the working principle behind [link-state routing protocols][77], [OSPF][14] and [IS-IS][13] being the most common ones.

Unlike Dijkstra's algorithm, the [Bellman–Ford algorithm][78] can be used on graphs with negative edge weights, as long as the graph contains no [negative cycle][79] reachable from the source vertex _s_. The presence of such cycles means there is no shortest path, since the total weight becomes lower each time the cycle is traversed. It is possible to adapt Dijkstra's algorithm to handle negative weight edges by combining it with the Bellman-Ford algorithm (to remove negative edges and detect negative cycles), such an algorithm is called [Johnson's algorithm][16].

The [A* algorithm][80] is a generalization of Dijkstra's algorithm that cuts down on the size of the subgraph that must be explored, if additional information is available that provides a lower bound on the "distance" to the target. This approach can be viewed from the perspective of [linear programming][81]: there is a natural [linear program for computing shortest paths][82], and solutions to its [dual linear program][83] are feasible if and only if they form a [consistent heuristic][34] (speaking roughly, since the sign conventions differ from place to place in the literature). This feasible dual / consistent heuristic defines a non-negative [reduced cost][84] and A* is essentially running Dijkstra's algorithm with these reduced costs. If the dual satisfies the weaker condition of [admissibility][85], then A* is instead more akin to the Bellman–Ford algorithm.

The process that underlies Dijkstra's algorithm is similar to the [greedy][86] process used in [Prim's algorithm][28]. Prim's purpose is to find a [minimum spanning tree][87] that connects all nodes in the graph; Dijkstra is concerned with only two nodes. Prim's does not evaluate the total weight of the path from the starting node, only the individual path.

[Breadth-first search][88] can be viewed as a special-case of Dijkstra's algorithm on unweighted graphs, where the priority queue degenerates into a FIFO queue.

[Fast marching method][89] can be viewed as a continuous version of Dijkstra's algorithm which computes the geodesic distance on a triangle mesh.

### Dynamic programming perspective[[edit][90]]

From a [dynamic programming][91] point of view, Dijkstra's algorithm is a successive approximation scheme that solves the dynamic programming functional equation for the shortest path problem by the **Reaching** method.[14][15][16]

In fact, Dijkstra's explanation of the logic behind the algorithm,[17] namely

&gt; **Problem 2.** Find the path of minimum total length between two given nodes ![P][92] and ![Q][48].
&gt;
&gt; We use the fact that, if ![R][93] is a node on the minimal path from ![P][92] to ![Q][48], knowledge of the latter implies the knowledge of the minimal path from ![P][92] to ![R][93].

is a paraphrasing of [Bellman's][94] famous [Principle of Optimality][95] in the context of the shortest path problem.

## See also[[edit][96]]

1. ^ _**a**_ _**b**_ Frana, Phil (August 2010). "An Interview with Edsger W. Dijkstra". _Communications of the ACM_ **53** (8): 41–47. [doi][97]:[10.1145/1787234.1787249][98]. What is the shortest way to travel from Rotterdam to Groningen? It is the algorithm for the shortest path which I designed in about 20 minutes. One morning I was shopping with my young fiancée, and tired, we sat down on the café terrace to drink a cup of coffee and I was just thinking about whether I could do this, and I then designed the algorithm for the shortest path.&nbsp;
2. ^ _**a**_ _**b**_ [Dijkstra, E. W.][10] (1959). ["A note on two problems in connexion with graphs"][99] (PDF). _Numerische Mathematik_ **1**: 269–271. [doi][97]:[10.1007/BF01386390][100].&nbsp;
3. ^ _**a**_ _**b**_ _**c**_ _**d**_ [Mehlhorn, Kurt][101]; [Sanders, Peter][102] (2008). _Algorithms and Data Structures: The Basic Toolbox_. Springer.&nbsp;
4. ^ _**a**_ _**b**_ _**c**_ Felner, Ariel (2011). [_Position Paper: Dijkstra's Algorithm versus Uniform Cost Search or a Case Against Dijkstra's Algorithm_][103]. Proc. 4th Int'l Symp. on Combinatorial Search.&nbsp; In a route-finding problem, Felner finds that the queue can be a factor 500–600 smaller, taking some 40% of the running time.
5. **^** Dijkstra, Edward W., [_Reflections on "A note on two problems in connexion with graphs_][104] (PDF)&nbsp;
6. **^** [Tarjan, Robert Endre][105] (1983), _Data Structures and Network Algorithms_, CBMS_NSF Regional Conference Series in Applied Mathematics **44**, Society for Industrial and Applied Mathematics, p.&nbsp;75, The third classical minimum spanning tree algorithm was discovered by Jarník and rediscovered by Prim and Dikstra; it is commonly known as Prim's algorithm.&nbsp;
7. **^** R. C. Prim: _Shortest connection networks and some generalizations_. In: _Bell System Technical Journal_, 36 (1957), pp.&nbsp;1389–1401.
8. **^** V. Jarník: _O jistém problému minimálním_ [About a certain minimal problem], Práce Moravské Přírodovědecké Společnosti, 6, 1930, pp.&nbsp;57–63. (in Czech)
9. **^** Chen, M.; Chowdhury, R. A.; Ramachandran, V.; Roche, D. L.; Tong, L. (2007). [_Priority Queues and Dijkstra's Algorithm — UTCS Technical Report TR-07-54 — 12 October 2007_][106] (PDF). Austin, Texas: The University of Texas at Austin, Department of Computer Sciences.&nbsp;
10. ^ _**a**_ _**b**_ [Russell, Stuart][107]; [Norvig, Peter][108] (2009) [1995]. [_Artificial Intelligence: A Modern Approach][109]_ (3rd ed.). Prentice Hall. pp.&nbsp;75, 81. [ISBN][110]&nbsp;[978-0-13-604259-4][111].&nbsp;
11. **^** Sometimes also _least-cost-first search_: Nau, Dana S. (1983). ["Expert computer systems"][112] (PDF). _Computer_ (IEEE) **16** (2): 63–85.&nbsp;
12. **^** <http: www.boost.org="" doc="" libs="" 1_44_0="" graph="" dag_shortest_paths.html="">
13. **^** Cormen etal, Introduction to Algorithms &amp; 3ed,chapter-24 2009
14. **^** Sniedovich, M. (2006). ["Dijkstra's algorithm revisited: the dynamic programming connexion"][113] ([PDF][114]). _Journal of Control and Cybernetics_ **35** (3): 599–620.&nbsp; [Online version of the paper with interactive computational modules.][115]
15. **^** Denardo, E.V. (2003). _Dynamic Programming: Models and Applications_. Mineola, NY: [Dover Publications][116]. [ISBN][110]&nbsp;[978-0-486-42810-9][117].&nbsp;
16. **^** Sniedovich, M. (2010). _Dynamic Programming: Foundations and Principles_. [Francis &amp; Taylor][118]. [ISBN][110]&nbsp;[978-0-8247-4099-3][119].&nbsp;
17. **^** Dijkstra 1959, p.&nbsp;270

## References[[edit][120]]

* [Cormen, Thomas H.][121]; [Leiserson, Charles E.][122]; [Rivest, Ronald L.][123]; [Stein, Clifford][124] (2001). "Section 24.3: Dijkstra's algorithm". [_Introduction to Algorithms][125]_ (Second ed.). [MIT Press][126] and [McGraw–Hill][127]. pp.&nbsp;595–601. [ISBN][110]&nbsp;[0-262-03293-7][128].&nbsp;
* [Fredman, Michael Lawrence][129]; [Tarjan, Robert E.][130] (1984). _Fibonacci heaps and their uses in improved network optimization algorithms_. 25th Annual Symposium on Foundations of Computer Science. [IEEE][131]. pp.&nbsp;338–346. [doi][97]:[10.1109/SFCS.1984.715934][132].&nbsp;
* [Fredman, Michael Lawrence][129]; [Tarjan, Robert E.][130] (1987). ["Fibonacci heaps and their uses in improved network optimization algorithms"][133]. _Journal of the Association for Computing Machinery_ **34** (3): 596–615. [doi][97]:[10.1145/28869.28874][134].&nbsp;
* Zhan, F. Benjamin; Noon, Charles E. (February 1998). "Shortest Path Algorithms: An Evaluation Using Real Road Networks". [_Transportation Science][135]_ **32** (1): 65–73. [doi][97]:[10.1287/trsc.32.1.65][136].&nbsp;
* Leyzorek, M.; Gray, R. S.; Johnson, A. A.; Ladew, W. C.; Meaker, Jr., S. R.; Petry, R. M.; Seitz, R. N. (1957). _Investigation of Model Techniques — First Annual Report — 6 June 1956 — 1 July 1957 — A Study of Model Techniques for Communication Systems_. Cleveland, Ohio: Case Institute of Technology.&nbsp;
* [Knuth, D.E.][137] (1977). "A Generalization of Dijkstra's Algorithm". [_Information Processing Letters][138]_ **6** (1): 1–5. [doi][97]:[10.1016/0020-0190(77)90002-3][139].&nbsp;
* Ahuja, Ravindra K.; Mehlhorn, Kurt; Orlin, James B.; Tarjan, Robert E. (April 1990). "Faster Algorithms for the Shortest Path Problem". _Journal of Association for Computing Machinery (ACM)_ **37** (2): 213–223. [doi][97]:[10.1145/77600.77615][140].&nbsp;
* Raman, Rajeev (1997). "Recent results on the single-source shortest paths problem". _SIGACT News_ **28** (2): 81–87. [doi][97]:[10.1145/261342.261352][141].&nbsp;
* Thorup, Mikkel (2000). "On RAM priority Queues". _SIAM Journal on Computing_ **30** (1): 86–109. [doi][97]:[10.1137/S0097539795288246][142].&nbsp;
* Thorup, Mikkel (1999). "Undirected single-source shortest paths with positive integer weights in linear time". _journal of the ACM_ **46** (3): 362–394. [doi][97]:[10.1145/316542.316548][143].&nbsp;

## External links[[edit][144]]

![][145]

[1]: https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif
[2]: /wiki/Search_algorithm "Search algorithm"
[3]: /wiki/Graph_(data_structure) "Graph (data structure)"
[4]: /wiki/Best,_worst_and_average_case "Best, worst and average case"
[5]: https://upload.wikimedia.org/math/8/a/f/8afcf9cf1e69f85b094063fd7b9e15cb.png
[6]: /wiki/Algorithm "Algorithm"
[7]: /wiki/Shortest_path_problem "Shortest path problem"
[8]: /wiki/Vertex_(graph_theory) "Vertex (graph theory)"
[9]: /wiki/Computer_scientist "Computer scientist"
[10]: /wiki/Edsger_W._Dijkstra "Edsger W. Dijkstra"
[11]: /wiki/Shortest_path_tree "Shortest path tree"
[12]: /wiki/Routing_protocol "Routing protocol"
[13]: /wiki/IS-IS "IS-IS"
[14]: /wiki/OSPF "OSPF"
[15]: /wiki/Subroutine "Subroutine"
[16]: /wiki/Johnson%27s_algorithm "Johnson's algorithm"
[17]: /wiki/Min-priority_queue "Min-priority queue"
[18]: /wiki/Time_complexity "Time complexity"
[19]: https://upload.wikimedia.org/math/1/7/5/17589e5af2a8e3d48c486d30939c1268.png
[20]: https://upload.wikimedia.org/math/7/4/f/74fcb594bdd93c0f956682ae1ea013e6.png
[21]: /wiki/Fibonacci_heap "Fibonacci heap"
[22]: https://upload.wikimedia.org/math/c/b/d/cbdcf0e203060323ebcd0079f280f4b8.png
[23]: /wiki/Asymptotic_computational_complexity "Asymptotic computational complexity"
[24]: /wiki/Directed_graph "Directed graph"
[25]: /wiki/Artificial_intelligence "Artificial intelligence"
[26]: /wiki/Best-first_search "Best-first search"
[27]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=1 "Edit section: History"
[28]: /wiki/Prim%27s_algorithm "Prim's algorithm"
[29]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=2 "Edit section: Algorithm"
[30]: https://upload.wikimedia.org/wikipedia/commons/2/23/Dijkstras_progress_animation.gif
[31]: /wiki/Robotics "Robotics"
[32]: /wiki/Motion_planning "Motion planning"
[33]: /wiki/Wavefront "Wavefront"
[34]: /wiki/Consistent_heuristic "Consistent heuristic"
[35]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=3 "Edit section: Description"
[36]: /wiki/Intersection_(road) "Intersection (road)"
[37]: /wiki/Graph_labeling "Graph labeling"
[38]: /wiki/Neighbourhood_(graph_theory) "Neighbourhood (graph theory)"
[39]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=4 "Edit section: Pseudocode"
[40]: /wiki/Depth-first_search "Depth-first search"
[41]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=5 "Edit section: Using a priority queue"
[42]: /wiki/Brodal_queue "Brodal queue"
[43]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=6 "Edit section: Proof of correctness"
[44]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=7 "Edit section: Running time"
[45]: https://upload.wikimedia.org/math/3/a/3/3a3ea00cfc35332cedf6e5e9a32e94da.png
[46]: https://upload.wikimedia.org/math/5/2/0/5206560a306a2e085a437fd258eb57ce.png
[47]: /wiki/Big_O_notation#Graph_theory "Big O notation"
[48]: https://upload.wikimedia.org/math/f/0/9/f09564c9ca56850d4cd6b3319e541aee.png
[49]: https://upload.wikimedia.org/math/f/d/2/fd205b17f1ae7a4e73fc46a167b29877.png
[50]: https://upload.wikimedia.org/math/8/6/5/8655969ac88a616b56743dc8ca4e5a06.png
[51]: https://upload.wikimedia.org/math/7/e/c/7ecfb0efd465c878e5dd1a5e0904d78a.png
[52]: https://upload.wikimedia.org/math/7/6/6/766e4baa0e84ddbf98dd44ec73c0723c.png
[53]: https://upload.wikimedia.org/math/3/0/4/304209ec202fce67f8973db90c2b9612.png
[54]: /wiki/Sparse_graph "Sparse graph"
[55]: https://upload.wikimedia.org/math/1/7/d/17d5fd6030715e7c6fd7cdef448d3e59.png
[56]: /wiki/Adjacency_list "Adjacency list"
[57]: /wiki/Self-balancing_binary_search_tree "Self-balancing binary search tree"
[58]: /wiki/Binary_heap "Binary heap"
[59]: /wiki/Pairing_heap "Pairing heap"
[60]: /wiki/Priority_queue "Priority queue"
[61]: https://upload.wikimedia.org/math/2/2/9/229c4d219a2dfaff648e0bf0124bed7a.png
[62]: https://upload.wikimedia.org/math/e/d/6/ed6417ed21f77d52a15b26e5a2ff6e83.png
[63]: /wiki/Probability_distribution "Probability distribution"
[64]: https://upload.wikimedia.org/math/6/a/3/6a3a4f70143d796236481abb59b62c50.png
[65]: https://upload.wikimedia.org/math/9/6/5/965e8454a0d3aacd459457e6e4e53988.png
[66]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=8 "Edit section: Practical optimizations and infinite graphs"
[67]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=9 "Edit section: Specialized variants"
[68]: https://upload.wikimedia.org/math/3/c/1/3c1a8075cf450df797f96fffeefd2759.png
[69]: /wiki/Radix_heap "Radix heap"
[70]: https://upload.wikimedia.org/math/4/e/1/4e18b32ab58b0d7b2a62477b9c8410b5.png
[71]: https://upload.wikimedia.org/math/c/b/1/cb1d00bae6b3948ee490040fef4f0e1d.png
[72]: https://upload.wikimedia.org/math/9/4/a/94af3fb93ad58da9635372acfe5cd8df.png
[73]: /wiki/Directed_acyclic_graph "Directed acyclic graph"
[74]: https://upload.wikimedia.org/math/9/6/b/96b64add48b0c59f6b07b490983a4a3b.png
[75]: https://upload.wikimedia.org/math/4/f/e/4fe9f15fb53c589e023213ae35fcf78f.png
[76]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=10 "Edit section: Related problems and algorithms"
[77]: /wiki/Link-state_routing_protocol "Link-state routing protocol"
[78]: /wiki/Bellman%E2%80%93Ford_algorithm "Bellman–Ford algorithm"
[79]: /wiki/Negative_cycle "Negative cycle"
[80]: /wiki/A-star_algorithm "A-star algorithm"
[81]: /wiki/Linear_programming "Linear programming"
[82]: /wiki/Shortest_path_problem#Linear_programming_formulation "Shortest path problem"
[83]: /wiki/Dual_linear_program "Dual linear program"
[84]: /wiki/Reduced_cost "Reduced cost"
[85]: /wiki/Admissible_heuristic "Admissible heuristic"
[86]: /wiki/Greedy_algorithm "Greedy algorithm"
[87]: /wiki/Minimum_spanning_tree "Minimum spanning tree"
[88]: /wiki/Breadth-first_search "Breadth-first search"
[89]: /wiki/Fast_marching_method "Fast marching method"
[90]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=11 "Edit section: Dynamic programming perspective"
[91]: /wiki/Dynamic_programming "Dynamic programming"
[92]: https://upload.wikimedia.org/math/4/4/c/44c29edb103a2872f519ad0c9a0fdaaa.png
[93]: https://upload.wikimedia.org/math/e/1/e/e1e1d3d40573127e9ee0480caf1283d6.png
[94]: /wiki/Richard_Bellman "Richard Bellman"
[95]: /wiki/Principle_of_Optimality "Principle of Optimality"
[96]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=12 "Edit section: See also"
[97]: /wiki/Digital_object_identifier "Digital object identifier"
[98]: //dx.doi.org/10.1145%2F1787234.1787249
[99]: http://www-m3.ma.tum.de/twiki/pub/MN0506/WebHome/dijkstra.pdf
[100]: //dx.doi.org/10.1007%2FBF01386390
[101]: /wiki/Kurt_Mehlhorn "Kurt Mehlhorn"
[102]: /wiki/Peter_Sanders_(computer_scientist) "Peter Sanders (computer scientist)"
[103]: http://www.aaai.org/ocs/index.php/SOCS/SOCS11/paper/view/4017/4357
[104]: https://www.cs.utexas.edu/users/EWD/ewd08xx/EWD841a.PDF
[105]: /wiki/Robert_Endre_Tarjan "Robert Endre Tarjan"
[106]: http://www.cs.sunysb.edu/~rezaul/papers/TR-07-54.pdf
[107]: /wiki/Stuart_J._Russell "Stuart J. Russell"
[108]: /wiki/Peter_Norvig "Peter Norvig"
[109]: /wiki/Artificial_Intelligence:_A_Modern_Approach "Artificial Intelligence: A Modern Approach"
[110]: /wiki/International_Standard_Book_Number "International Standard Book Number"
[111]: /wiki/Special:BookSources/978-0-13-604259-4 "Special:BookSources/978-0-13-604259-4"
[112]: https://www.cs.umd.edu/~nau/papers/nau1983expert.pdf
[113]: http://matwbn.icm.edu.pl/ksiazki/cc/cc35/cc3536.pdf
[114]: /wiki/PDF "PDF"
[115]: http://www.ifors.ms.unimelb.edu.au/tutorial/dijkstra_new/index.html
[116]: /wiki/Dover_Publications "Dover Publications"
[117]: /wiki/Special:BookSources/978-0-486-42810-9 "Special:BookSources/978-0-486-42810-9"
[118]: /w/index.php?title=Francis_%26_Taylor&amp;action=edit&amp;redlink=1 "Francis &amp; Taylor (page does not exist)"
[119]: /wiki/Special:BookSources/978-0-8247-4099-3 "Special:BookSources/978-0-8247-4099-3"
[120]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=14 "Edit section: References"
[121]: /wiki/Thomas_H._Cormen "Thomas H. Cormen"
[122]: /wiki/Charles_E._Leiserson "Charles E. Leiserson"
[123]: /wiki/Ronald_L._Rivest "Ronald L. Rivest"
[124]: /wiki/Clifford_Stein "Clifford Stein"
[125]: /wiki/Introduction_to_Algorithms "Introduction to Algorithms"
[126]: /wiki/MIT_Press "MIT Press"
[127]: /wiki/McGraw%E2%80%93Hill "McGraw–Hill"
[128]: /wiki/Special:BookSources/0-262-03293-7 "Special:BookSources/0-262-03293-7"
[129]: /wiki/Michael_Fredman "Michael Fredman"
[130]: /wiki/Robert_Tarjan "Robert Tarjan"
[131]: /wiki/IEEE "IEEE"
[132]: //dx.doi.org/10.1109%2FSFCS.1984.715934
[133]: http://portal.acm.org/citation.cfm?id=28874
[134]: //dx.doi.org/10.1145%2F28869.28874
[135]: /wiki/Transportation_Science "Transportation Science"
[136]: //dx.doi.org/10.1287%2Ftrsc.32.1.65
[137]: /wiki/Donald_Knuth "Donald Knuth"
[138]: /wiki/Information_Processing_Letters "Information Processing Letters"
[139]: //dx.doi.org/10.1016%2F0020-0190%2877%2990002-3
[140]: //dx.doi.org/10.1145%2F77600.77615
[141]: //dx.doi.org/10.1145%2F261342.261352
[142]: //dx.doi.org/10.1137%2FS0097539795288246
[143]: //dx.doi.org/10.1145%2F316542.316548
[144]: /w/index.php?title=Dijkstra%27s_algorithm&amp;action=edit§ion=15 "Edit section: External links"
[145]: //en.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1 ""
  </http:>
