// Arup Guha

// 6/25/02
// Floyd Warshall's algorithm: an example of dynamic programming.
import java.util.*;
public class FloydsProto {
   public static double[][] shortestpath(double[][] adj, double[][] path, int arraySize) {

      double[][] ans = new double[arraySize][arraySize];

      // Implement algorithm on a copy matrix so that the adjacency isn't
      // destroyed.
      copy(ans, adj);

      // Compute successively better paths through vertex k.
      for (int k = 0; k < arraySize; k++) {
         // Do so between each possible pair of points.
         for (int i = 0; i < arraySize; i++) {
            for (int j = 0; j < arraySize; j++) {
               if (ans[i][k] + ans[k][j] < ans[i][j]) {
                  ans[i][j]  = ans[i][k] + ans[k][j];
                  path[i][j] = path[k][j];
               }
            }
         }
      }

      // Return the shortest path matrix.
      return ans;
   }

   // Copies the contents of array b into array a. Assumes that both a and b are 2D arrays of identical dimensions.
   public static void copy(double[][] a, double[][] b) {
      for (int i = 0; i < a.length; i++) {
         for (int j = 0; j < a[0].length; j++) {
            a[i][j] = b[i][j];
         }
      }
   }

   // Returns the smaller of a and b.
   public static int min(int a, int b) {
      if (a < b) {
         return a;
      } else {
         return b;
      }
   }

   public void Floyds(double graph[][], int arraySize) {
      double[][] shortpath;
      double[][] path = new double[arraySize][arraySize];

      // Initialize with the previous vertex for each edge. -1 indicates
      // no such vertex.
      for (int i = 0; i < arraySize; i++) {
         for (int j = 0; j < arraySize; j++) {
            if (graph[i][j] == Double.POSITIVE_INFINITY) {
               path[i][j] = -1;
            } else {
               path[i][j] = i;
            }
         }
      }

      // This means that we don't have to go anywhere to go from i to i.
      for (int i = 0; i < arraySize; i++) {
         path[i][i] = i;
      }

      shortpath = shortestpath(graph, path, arraySize);

      // Prints out shortest distances.
      for (int i = 0; i < arraySize; i++) {
         for (int j = 0; j < arraySize; j++) {
            System.out.print(shortpath[i][j] + " ");
         }

         System.out.println();
      }

      //for (int s = 0; s < arraySize; s++) {
      //   for (int e = 0; e < arraySize; e++) {
            //loop(s+1, (e+1), path);
            loop(0,1,path);
      //   }
      //}
   }

   public void loop(int start, int end, double path[][]){
      String myPath = end + "";

      // Loop through each previous vertex until you get back to start.
      System.out.println();
      while (path[start][end] != (double)start) {
         myPath = path[start][end] + " -> " + myPath;
         end    = (int)path[start][end];
      }

      // Just add start to our string and print.
      myPath = start + " -> " + myPath;
      System.out.println("Here's the path " + myPath);
   }

}
