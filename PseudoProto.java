import java.util.*;
import java.io.*;

/**
 * File: Polish.java
 * Machine Problem # 1
 *
 * Members: Rosheil Parel
 *          Grygjeanne Grace Icay
 *          Anfernee Sodusta
 *
 */

/**
 * class Vertex = Stores cost and adjacency list of vertex
 */
class Vertex {
   ArrayList<Integer> posOfAdj = new ArrayList<Integer>();
   ArrayList<Double>  cost     = new ArrayList<Double>();

   /**
    * Main method of Vertex class that stores adjacency list or the position of the adjacency vertex
    *@param duble[][] graph. The main graph graph
    *@param int vertex. on What vertex or row
    *@param int graphSize. Determines the size of the graph, preferrably the last vertex
    */
   Vertex(double[][] graph, int vertex, int graphSize){

      for (int x = 0; x < graphSize; x++) {
         try {
            if ((graph[vertex][x] != 0) && (graph[vertex][x] != Double.POSITIVE_INFINITY)) {
               cost.add(graph[vertex][x]);
               posOfAdj.add(x + 1);
            }
         }catch (Exception e) {

         }
      }
   }

}

/**
 * Method that processes the Graph and stores predecessor to be used by the main dijkstras and floyds algo
 */
class Graph {
   ArrayList<Vertex> vertices = new ArrayList<Vertex>();
   double copyy[][];
   int    pred[];

   /**
    * Method that copies the "parsed" graph and stores predecessors of vertex
    *@param double parsedGraph[][]. The main graph/matrix
    *@param int graphSize. Size of the graph or number of vertx
    */
   Graph(double parsedGraph[][], int graphSize){
      copyy = new double[graphSize][graphSize];
      pred  = new int[graphSize];

      for (int i = 0; i < graphSize; i++) {
         for (int j = 0; j < graphSize; j++) {
            copyy[i][j] = parsedGraph[i][j];
         }
      }

      pred = new int[graphSize];

      for (int i = 0; i < graphSize; i++) {
         Vertex v = new Vertex(copyy, i, graphSize);
         vertices.add(v);
      }
   }

}

/**
 * Main class of the program
 */
public class PseudoProto {

   /**
    * Parses the text file and runs the dijkstras and floyds Algorithm
    */
   public static void main(String[] args) {
      double   parsedGraph[][] = new double[10][10];
      int      x = 0;
      int      arraySize;
      String[] countt = {" "};

      Scanner scan = new Scanner(System.in);

      System.out.print("\nEnter file name> ");
      String filename = "graph4.txt";

      try{
         File file = new File(filename);
         FileReader     fr = new FileReader(file);
         BufferedReader br = new BufferedReader(fr);

         while (countt != null) {
            countt = (br.readLine()).split(" ");

            for (int y = 0; y < countt.length; y++) {
               if (countt[y].equals("x")) {
                  parsedGraph[x][y] = Double.POSITIVE_INFINITY;
               } else {
                  parsedGraph[x][y] = Double.parseDouble(countt[y]);
               }
            }

            x++;
         }
      }catch (Exception e) {}
      arraySize = countt.length;

      Graph graph = new Graph(parsedGraph, arraySize);

      /*
            DijkstrasProto dij = new DijkstrasProto();

            System.out.println("\n\nDijkstra's Algorithm\n");

            for (int z = 1; z <= arraySize; z++) {
               dij.dijkstra(graph, z, arraySize);
            }

       */
      System.out.println("\n\nFloyds's Algorithm\n");


      FloydsProto floyds = new FloydsProto();
      floyds.Floyds(parsedGraph, arraySize);

   }

}
