public class Node {
   private int distances[];
   private Set<Integer> settled;
   private Set<Integer> unsettled;
   private int number_of_nodes;
   private int adjacencyMatrix[][];

   public Node(int number_of_nodes){
      this.number_of_nodes = number_of_nodes;
      distances = new int[number_of_nodes + 1];
      settled   = new HashSet<Integer>();
      unsettled = new HashSet<Integer>();
      adjacencyMatrix = new int[number_of_nodes + 1][number_of_nodes + 1];
   }



}
